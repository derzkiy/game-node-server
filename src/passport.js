import JwtPassport from 'passport-jwt';
import Users from './app/users/model/User';
import jwtOptions from './config/jwt-options';

let JwtStrategy = JwtPassport.Strategy;

export default new JwtStrategy(jwtOptions, function(jwt_payload, next) {
    Users.findOne({_id: jwt_payload.id}).exec()
        .then((doc) => {
            if (doc) {
                return next(null, doc);
            }
            return next(null, false);
        })
    ;
});