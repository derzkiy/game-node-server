import PassportJwt from "passport-jwt";

let ExtractJwt = PassportJwt.ExtractJwt;

let jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = 'secretHash';

export default jwtOptions;