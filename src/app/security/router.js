import Express from 'express';
import Jwt from 'jsonwebtoken';
import Security from './security';
let router = Express.Router();

router.post('/registration', (req, res) => {

    let user =  {
        lastName: req.body.last_name,
        firstName: req.body.first_name,
        email: req.body.email
    };
    let password = req.body.password;

    return Security.registration(user, password)
        .then(() => {
            return res.status(200).json({
                message: 'ok',
            })
        })
        .catch((err) => {
            return res.status(400).json({
                message: err,
            })
        })
    ;
});

router.post('/auth', (req, res) => {
    let email = req.body.email;
    let password = req.body.password;

    return Security.authorization(email, password)
        .then((user) => {
            let token = Jwt.sign(user.toObject(), '123123', {
                expiresIn: 60 * 24
            });
            return res.status(200).json({
                message: 'ok',
                token
            })
        })
        .catch((err) => {
            return res.status(401).json({error:err})
        })
    ;
});

export default router;