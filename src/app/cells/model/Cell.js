import db from './../../database/app';

let Schema = db.Schema;

let CellModel = new Schema({
    prize: Boolean,
    userId: Number,
    position: Number,
});

let Cell = db.model('Cell', CellModel);

export default Cell;