import { Schema } from 'mongoose';
import sha256 from 'crypto-js/sha256';
import db from './../../database/app';

let UserSchema = new Schema({
    id: Number,
    firstName: String,
    lastName: String,
    email: String,
    createdAt: Date,
    pwdHash: String,
    salt: String
});

/**
 * @see https://github.com/Automattic/mongoose/issues/5057
 * Schema statics method context(this) is empty when using es6 arrow function
 */
UserSchema.methods.checkPassword = function(password) {
    return password && this.pwdHash === sha256(password + this.salt).toString()
};

let User = db.model('User', UserSchema);


export default User;