import User from './../users/model/User';
import Crypto from 'crypto';
import sha256 from 'crypto-js/sha256';

let Security = {
    registration(user, password) {
        if (user.email) {
            return this.findUserByEmail(user.email)
                .then(doc => {
                    if (!doc) {
                        return Promise.resolve();
                    }
                    return Promise.reject('User exists');
                })
                .then(() => {
                    let salt = Crypto.randomBytes(6).toString('hex');
                    return new User({
                        email: user.email,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        pwdHash: sha256(password + salt),
                        salt: salt
                    }).save();
                });
        }
    },

    authorization(email, password) {
        return this.findUserByEmail(email)
            .then((doc) => {
                if (doc && doc.checkPassword(password)) {
                    return Promise.resolve(doc)
                }

                return Promise.reject('User not found')
            })

        ;
    },

    findUserByEmail(email) {
        return User.findOne({email: email}).exec();
    }
};

export default Security;