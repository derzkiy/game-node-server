import Mongoose from 'mongoose';
import Bluebird from 'bluebird';

let uri = 'mongodb://localhost/node-server';
let options = {
    promiseLibrary: Bluebird
};

Mongoose.Promise = Bluebird;
Mongoose.set('debug', true);

let Db = Mongoose.createConnection(uri, options);

export default Db;