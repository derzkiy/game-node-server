import Express from 'express';
import BodyParser from 'body-parser';
import Users from './app/users/router';
import Cells from './app/cells/router';
import Security from './app/security/router';
import Passport from 'passport';
import Strategy from './passport';
import Cors from 'cors';

let app = Express();
let port = 3000;

Passport.use(Strategy);
app.use(Passport.initialize());

app.use(BodyParser.urlencoded({
    extended: true
}));
app.use(BodyParser.json());

app.use(Cors());

app.use('/', Security);
app.use('/user', Users);
app.use('/cell', Cells);

app.listen(port, 'game.server.dev', () => {
    console.log('example app');
});