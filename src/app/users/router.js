import Express from 'express';
let router = Express.Router();

router.use(function timeLog(req, res, next) {
    let date = new Date(Date.now());
    console.log('Time: ', date);
    next();
});

router.get('/', (req, res) => {
    res.send('user list');
});

router.get('/:id', (req, res) => {
    res.send(req.params);
});

export default router;